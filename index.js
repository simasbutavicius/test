import express from "express";
const app = express();
app.use(express.json());

const foo = process.env.FOO || "n/a";
const bar = process.env.BAR || "n/a";

app.get("/", (req, res) => {
  res.json({ message: "Hello World", foo, bar });
});

app.listen(5000, () => {
  console.log("Server is running on port 5000");
});
